package steps;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import static io.restassured.RestAssured.*;
import static org.hamcrest.CoreMatchers.equalTo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import static org.hamcrest.Matchers.*;

public class PetStep {

	@Given("^user logs into site$")
	public void user_logs_into_site() throws Throwable {
		WebDriver driver = null;
	
	   if(Hooks.browser.equals("chrome")) {
		   System.setProperty("webdriver.chrome.driver", "src/main/resource/chromedriver.exe");
		   driver=new ChromeDriver();
		 
	   }else if (Hooks.browser.equals("ie")) {
		   System.setProperty("webdriver.ie.driver", "src/main/resource/IEDriverServer.exe");
		   driver=new InternetExplorerDriver();
		   DesiredCapabilities capability = DesiredCapabilities.internetExplorer();
		   capability.setBrowserName("internet explorer");
		   capability.setCapability("platformName", "WINDOWS");
		  
	}
	   driver.get("https://www.google.com/");
	   
	   driver.quit();
	}
	}

